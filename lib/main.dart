import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

final titles = [
  "Colette",
  "El-Primo",
  "Spike",
  "Bea",
  "Brock",
  "Poco",
  "Piper",
  "Nita",
  "Rico",
  "Bibi",
  "Tara",
  "Surge",
  "Penny",
  "Barley",
  "Jessie",
  "Tick",
  "Darryl",
  "Nani",
  "Colt",
  "Gene",
  "Jacky",
  "Max",
  "Bo",
  "Dynamike",
  "Stu",
  "Emz",
  "Carl",
  "Gale",
  "Lou",
  "Byron",
  "Sprout",
  "Frank",
  "Mr.P",
  "Mortis",
  "Buzz",
  "Crow",
  "Rosa",
  "Shelly",
  "8-Bit",
  "Bull",
  "Pam",
  "Belle",
  "Griff",
  "Ash",
  "Grom",
  "Gus",
  "Eve",
  "Squeak"
];

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: titles.length,
        itemBuilder: (context, index) {
          return Card(
              semanticContainer: true,
              margin: EdgeInsets.all(10),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: ListTile(
                  title: Text(
                    titles[index],
                    style: TextStyle(fontSize: 25),
                  ),
                  subtitle: Text("subtitle${index}"),
                  leading: ConstrainedBox(
                      constraints: const BoxConstraints(
                          minHeight: 90, minWidth: 70),
                      child: Image.network(
                        fit: BoxFit.fill,
                        "https://cdn.brawlify.com/brawler-bs/${titles[index]}.png",
                      ))));
        });
  }
}
